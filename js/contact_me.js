$(function() {

  $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
    preventSubmit: true,
    submitError: function($form, event, errors) {
      // additional error messages or events
    },
    submitSuccess: function($form, event) {
      event.preventDefault(); // prevent default submit behaviour
      // get values from FORM
      var name = $("input#name").val();
      var email = $("input#email").val();
      var phone = $("input#phone").val();
      var message = $("textarea#message").val();
      var firstName = name; // For Success/Failure Message
      var username = "codeartsoftdev@gmail.com";
      // Check for white space in name for Success/Fail message
      if (firstName.indexOf(' ') >= 0) {
        firstName = name.split(' ').slice(0, -1).join(' ');
      }
      $this = $("#sendMessageButton");
      $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
      /* console.log("a punto de invocar el envio de correo"); */

      setTimeout(function() {
        $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
      }, 2000);
      
      /* Email.send({
        Host : "smtp.gmail.com",
        Username : username,
        Password : "TripaBucheTuetano", */

        var template = GetEmailTemplate(name, email, phone, message);

        Email.send({
        SecureToken : "5ffab7e9-7ee0-464f-b1a0-bca43bf6cf24",
        To : "Mexico Drilling Services " + username,
        Bcc : email,
        From : username,
        FromName: "Mexico Drilling Services",
        //ReplyAddress: "noreply@mdrills.com",
        Subject : "Contacto desde formulario MDS",
        Body : template //message + " comunicate al " + phone
    }).then(
      /* message => alert(message) */
      function (message){
         //console.log(message);
        /* alert(message);    */
        if(message == 'OK')
        {
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success')
            .append("<strong>Tu mensaje ha sido enviado, en breve nos pondremos en contacto. </strong>");
          $('#success > .alert-success')
            .append('</div>');
          $("#success").fadeIn("slow").delay(4000).fadeOut("slow");
          $('#contactForm').trigger("reset");
        }
        else{
          $('#success').html("<div class='alert alert-danger'>");
          /* $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            .append("</button>"); */
          $('#success > .alert-danger').append($("<strong>").text("Una disculpa " + firstName + ", todo parece indicar que el servidor de correo no responde. Por favor, intenta mas tarde!"));
          $('#success > .alert-danger').append('</div>');
          $("#success").fadeIn("slow").delay(4000).fadeOut("slow");
          $('#contactForm').trigger("reset");
        }
      }
    );
    },
    filter: function() {
      return $(this).is(":visible");
    },
  });

  $("a[data-toggle=\"tab\"]").click(function(e) {
    e.preventDefault();
    $(this).tab("show");
  });
});

/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
  $('#success').html('');
});

function GetEmailTemplate(name, email, phone, message){

  //var data = name + ' - ' + email + ' - ' + phone + ' - ' + message + ' - ' + username + ' - '

var data = `

<html>

<body>
    <div>
        <table style="max-width: 500px; min-width: 350px;">
            <tbody>
                <tr>
                    <td style="text-align: center; padding-top:10px; font-size:22px; font-weight: bold; color:#031B61;">
                        Mexico Drilling Services
                        <br>
                        <span style='font-size:18px; font-weight: bold; color:#F37F06'>
                            Atención a correo
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;"">
                                <hr style=" background: rgba(3, 27, 97, 1); background: -moz-linear-gradient(-45deg,
                        rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background: -webkit-gradient(left top, right
                        bottom, color-stop(0%, rgba(3, 27, 97, 1)), color-stop(100%, rgba(243, 128, 6, 1))); background:
                        -webkit-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        -o-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        -ms-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        linear-gradient(135deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); filter:
                        progid:DXImageTransform.Microsoft.gradient(startColorstr='#031b61' , endColorstr='#f38006' ,
                        GradientType=1);" size=" 10" noshade>
                    </td>
                </tr>
                <tr>
                    <td
                        style='text-align: center; padding:0; padding-top:20px; font-size:18px; font-weight: bold; color:#505359;'>
                        Hola, ${name}!
                    </td>
                </tr>
                <tr>
                    <td
                        style='text-align: center; padding-bottom: 20px; padding-top:10px; font-size:18px; font-weight: normal; color:#505359;'>
                        Recibimos tu información.
                    </td>
                </tr>
                <br>
                <tr>
                    <td style="text-align: left; padding-top:10px; font-size:16px; font-weight: bold; color:#505359;">
                        Email:
                        <span style='font-size:16px; font-weight: normal; color:#505359'>
                              ${email}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-top:10px; font-size:16px; font-weight: bold; color:#505359;">
                        Teléfono:
                        <span style='font-size:16px; font-weight: normal; color:#505359'>
                              ${phone}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-top:10px; font-size:16px; font-weight: bold; color:#505359;">
                        Mensaje:
                        <span style='font-size:16px; font-weight: normal; color:#505359'>
                              ${message}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:20px;"">
                            <hr style=" background: rgba(3, 27, 97, 1); background: -moz-linear-gradient(-45deg,
                        rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background: -webkit-gradient(left top, right
                        bottom, color-stop(0%, rgba(3, 27, 97, 1)), color-stop(100%, rgba(243, 128, 6, 1))); background:
                        -webkit-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        -o-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        -ms-linear-gradient(-45deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); background:
                        linear-gradient(135deg, rgba(3, 27, 97, 1) 0%, rgba(243, 128, 6, 1) 100%); filter:
                        progid:DXImageTransform.Microsoft.gradient(startColorstr='#031b61' , endColorstr='#f38006' ,
                        GradientType=1);" size=" 10" noshade>
                    </td>
                </tr>
                <tr>
                    <td
                        style='text-align: center; padding-bottom: 10px; padding-top:10px; font-size:18px; font-weight: normal; color:#505359;'>
                        En breve, un asesor te contactará
                    </td>
                </tr>
                <tr>
                    <td style='text-align: center; padding-top:10px; font-size:18px; font-weight: bold; color:#505359;'>
                        ¡Gracias!
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>

`;


  return data;
};